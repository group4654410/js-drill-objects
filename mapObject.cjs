function mapObject(obj, cb) {
    let mappedObjects = {};
    for (const [key, value] of Object.entries(obj)) {
        mappedObjects[key] = cb(value, key);
    }
    return mappedObjects;
}

module.exports = mapObject;