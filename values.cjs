function values(obj) {
    let values = []
    for (const [key, value] of Object.entries(obj)) {
        values.push(String(value));
    }
    return values;
}

module.exports = values;