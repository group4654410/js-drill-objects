function keys(obj) {
    let keys = []
    for (const [key, value] of Object.entries(obj)) {
        keys.push(String(key));
    }
    return keys;
}

module.exports = keys;