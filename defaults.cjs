function defaults(obj, defaultProps) {
    let newProps = {};
    for (const [key, value] of Object.entries(defaultProps)) {
        if(key in obj) {
            newProps[key] = obj[key];
        } else {
            newProps[key] = value;
        }
    }
    return newProps;
}

module.exports = defaults;