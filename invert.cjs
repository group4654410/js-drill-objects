function invert(obj) {
    let invertedObj = {};
    for (const [key, value] of Object.entries(obj)) {
        invertedObj[value] = key;
    }
    return invertedObj;
}

module.exports = invert;