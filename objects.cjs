const keys = require("./keys.cjs");
const values = require("./values.cjs");
const mapObject = require("./mapObject.cjs");
const pairs = require("./pairs.cjs");
const invert = require("./invert.cjs");
const defaults = require("./defaults.cjs");

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

// console.log(keys(testObject));
// console.log(values(testObject));
// console.log(mapObject(testObject, (val, key) => { return String(val) }));
// console.log(pairs(testObject));
// console.log(invert(testObject));
// console.log(defaults({age: 23}, testObject));