const assert = require("assert");
const values = require("../values.cjs");

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const result = ['Bruce Wayne', '36', 'Gotham'];
assert.deepStrictEqual(values(testObject), result);