const assert = require("assert");
const pairs = require("../pairs.cjs");

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const result = [['name', 'Bruce Wayne'], ['age', 36], ['location', 'Gotham']];
assert.deepStrictEqual(pairs(testObject), result);