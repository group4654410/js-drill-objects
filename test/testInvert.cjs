const assert = require("assert");
const invert = require("../invert.cjs");

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const result = { '36': 'age', 'Bruce Wayne': 'name', Gotham: 'location' };
assert.deepStrictEqual(invert(testObject), result);