const assert = require("assert");
const defaults = require("../defaults.cjs");

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const result = { name: 'Bruce Wayne', age: 23, location: 'Gotham' };
assert.deepStrictEqual(defaults({age: 23}, testObject), result);