const assert = require("assert");
const mapObject = require("../mapObject.cjs");

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const result = { name: 'Bruce Wayne', age: '36', location: 'Gotham' };
assert.deepStrictEqual(mapObject(testObject, (val, key) => { return String(val) }), result);