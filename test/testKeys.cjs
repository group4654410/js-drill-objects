const assert = require("assert");
const keys = require("../keys.cjs");

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const result = ['name', 'age', 'location'];
assert.deepStrictEqual(keys(testObject), result);