function pairs(obj) {
    let pairList = [];
    for (const [key, value] of Object.entries(obj)) {
        pairList.push([key, value]);
    }
    return pairList;
}

module.exports = pairs;